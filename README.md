# Photo Gallery

Photo Gallery est une application React qui permet aux utilisateurs de déposer des images, de les afficher en grand format et de les supprimer. Cependant, veuillez noter que toutes les images disparaissent lors du rechargement de la page car elles ne sont pas stockées de manière permanente dans une base de données ou un espace de stockage externe.

***

## Fonctionnalités

- Ajout d'images à la galerie.
- Affichage des images en grand format lorsqu'elles sont sélectionnées.
- Suppression d'images de la galerie.

***

## Installation

Pour utiliser l'application Photo Gallery, suivez ces étapes :

1. Clonez le dépôt du projet sur votre ordinateur :

   ```shell
   git clone https://gitlab.com/arthur.delaporte/photo-gallery.git
   ```

2. Accédez au répertoire du projet :

   ```shell
   cd photo-gallery
   ```

3. Installez les dépendances du projet :

   ```shell
   npm install
   ```

4. Lancez l'application :

   ```shell
   npm start
   ```

5. L'application sera accessible dans votre navigateur à l'adresse http://localhost:3000.

***

## Avertissement

Veuillez noter que toutes les images ajoutées à la galerie ne sont pas stockées de manière permanente. Une fois la page rechargée, toutes les images seront perdues. Pour conserver les images de manière permanente, vous devrez mettre en place une solution de stockage de fichiers ou utiliser une base de données pour enregistrer les références aux images téléchargées.

***

## Contribuer

Les contributions au projet Photo Gallery sont les bienvenues ! Si vous souhaitez contribuer, suivez ces étapes :

1. Clonez le dépôt Photo Gallery sur votre machine locale :

   ```shell
   git clone https://gitlab.com/arthur.delaporte/photo-gallery.git
   ```

2. Créez une branche pour vos modifications :

   ```shell
   git checkout -b feature/nom-de-votre-branche
   ```

3. Effectuez vos modifications en vous assurant qu'elles respectent les conventions de codage du projet.

4. Testez soigneusement vos modifications pour vous assurer qu'elles fonctionnent correctement.

5. Validez vos modifications et poussez-les vers votre dépôt cloné.

6. Créez une demande de fusion (merge request) pour proposer vos modifications à la revue et à l'intégration dans le projet principal.

Nous apprécions grandement les contributions sous forme de corrections de bogues, d'améliorations de fonctionnalités ou de nouvelles fonctionnalités. Cependant, veuillez garder à l'esprit que, pour conserver les images de manière permanente, des ajustements au stockage des fichiers ou l'utilisation d'une base de données peuvent être nécessaires.

Avant de soumettre votre contribution, assurez-vous de bien documenter vos changements et de suivre les meilleures pratiques du projet.

***

## Auteur

Ce projet a été réalisé par [Arthur Delaporte](https://gitlab.com/arthur.delaporte)
