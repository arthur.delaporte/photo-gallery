import React, { useState } from "react";
import "./index.css";

function App() {
  const [photos, setPhotos] = useState([]);

  // Gestion de l'image qui a été ajoutée par drag en drop
  const handleDrop = (e) => {
    e.preventDefault();
    const file = e.dataTransfer.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      if (photos.length >= 50) {
        const newPhotos = photos.slice(1);
        setPhotos(newPhotos);
        localStorage.setItem("photos", JSON.stringify(newPhotos));
      }

      const newPhotos = [...photos, reader.result];
      setPhotos(newPhotos);
      localStorage.setItem("photos", JSON.stringify(newPhotos));
    };
  };

  // Gestion de l'image qui a été ajoutée par sélection
  const handleFileSelect = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      if (photos.length >= 50) {
        const newPhotos = photos.slice(1);
        setPhotos(newPhotos);
        localStorage.setItem("photos", JSON.stringify(newPhotos));
      }

      const newPhotos = [...photos, reader.result];
      setPhotos(newPhotos);
      localStorage.setItem("photos", JSON.stringify(newPhotos));
    };
  };

  // Gestion de la suppression d'image
  const handleDelete = (index) => {
    const newPhotos = [...photos];
    newPhotos.splice(index, 1);
    setPhotos(newPhotos);
    localStorage.setItem("photos", JSON.stringify(newPhotos));
  };

  const [fullscreen, setFullscreen] = useState(false);
  const [fullscreenImage, setFullscreenImage] = useState("");

  // Gestion du mode plein écran
  const handleFullscreen = (photo) => {
    setFullscreenImage(photo);
    setFullscreen(true);
  };

  // Gestion de la sortie du mode plein écran
  const handleCloseFullscreen = () => {
    setFullscreenImage("");
    setFullscreen(false);
  };

  // Rendu de la grille d'images
  const renderPhotos = () => {
    if (photos.length === 0) {
      return <p>Aucune image disponible</p>;
    }
    return photos.map((photo, index) => {
      return (
          <div key={index} className="photo-container">
            <img
                src={photo}
                alt=""
                onClick={() => handleFullscreen(photo)}
                className="photo"
            />
            <button onClick={() => handleDelete(index)} className="delete-btn">
              Supprimer
            </button>
          </div>
      );
    });
  };

  // Rendu de l'image en mode plein écran
  const renderFullscreenImage = () => {
    if (fullscreen) {
      return (
          <div className="fullscreen-container" onClick={handleCloseFullscreen}>
            <img src={fullscreenImage} alt="" className="fullscreen-image" />
          </div>
      );
    }
    return null;
  };

  // Rendu de l'application avec tous les éléments
  return (
      <div className="app">
        <div className="header">
          <h1 className="title">Photo Gallery by Arthur DELAPORTE</h1>
        </div>
        <div className="drop-zone" onDragOver={(e) => e.preventDefault()} onDrop={handleDrop}>
          <p style={{ marginBottom: "0px" }}><b>Glissez-déposez une image</b></p>
          <p style={{ marginBottom: "3px" }}>OU</p>
          <label htmlFor="file-input" className="select-btn">Sélectionnez une image</label>
          <input type="file" id="file-input" accept="image/*" onChange={handleFileSelect} className="file-input" />
        </div>
        <div className="photo-grid">{renderPhotos()}</div>
        {renderFullscreenImage()}
      </div>
  );
}

export default App;
